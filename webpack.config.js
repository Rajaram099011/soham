const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ScriptExtPlugin = require('script-ext-html-webpack-plugin');
const { AngularCompilerPlugin } = require('@ngtools/webpack');
const isDev = process.env.NODE_ENV === 'development';
//const NodemonPlugin = require( 'nodemon-webpack-plugin' )

module.exports = function () {
    return {
        mode: isDev ? 'development' : 'production',
        entry: './src/client/index.ts',
        output: {
            path: __dirname + '/dist',
            filename: 'app.js'
        },
        resolve: {
            extensions: ['.ts', '.js']
        },
        module: {
            rules: [
                {test: /\.ts$/, loaders: ['@ngtools/webpack']},
                { test: /\.css$/, loader: 'raw-loader' },
                { test: /\.html$/, loader: 'raw-loader' }
            ]
        },
        devServer: {
            contentBase: './dist'
        },
        plugins: [
            //new NodemonPlugin(),
            new CopyWebpackPlugin([
                { from: 'src/client/assets', to: 'assets'}
            ]),
            new HtmlWebpackPlugin({
                template: __dirname + '/src/client/index.html',
                output: __dirname + '/dist',
                inject: 'head'
            }),
            new ScriptExtPlugin({
                defaultAttribute: 'defer'
            }),
            new AngularCompilerPlugin({
                tsConfigPath: './tsconfig.json',
                entryModule: './src/client/app.module#AppModule',
                sourceMap: true
            })
        ]
    };
};
